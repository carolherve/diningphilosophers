public class Philosopher extends Thread {
  private GraphicTable table;
  private int left;
  private int right;
  private int ID;
  final int timeThink_max = 1000;
  final int timeNextFork = 100;
  final int timeEat_max = 2000;

  
  Philosopher(int ID, GraphicTable table, int left, int right) {
    this.ID = ID;
    this.table = table;
    this.left = left;
    this.right = right;
    setName("Philosopher "+ID);
  }

public void run() {
    for(;;) {

    	table.isThinking(ID);
        System.out.println(getName()+" thinks");
        try {

        	sleep((long)(Math.random()*timeThink_max));

        	} catch(InterruptedException e) {
        	System.out.println(e);
        }        
        System.out.println(getName()+" finished thinking");               
 
    System.out.println(getName()+" is hungry");               
      table.isHungry(ID);

      System.out.println(getName()+" wants left chopstick");
      table.take(left);
      table.takeChopstick(ID, left);
      System.out.println(getName()+" got left chopstick");
      
      try {
	sleep(timeNextFork);
      } catch(InterruptedException e) {
	System.out.println(e);
      }     
      
      System.out.println(getName()+" wants right chopstick");
      table.take(right);
      table.takeChopstick(ID, right);
      System.out.println(getName()+" got right chopstick");

      System.out.println(getName()+" eats");               
      try {
	sleep((long)(Math.random()*timeEat_max));
      } catch(InterruptedException e) {
	System.out.println(e);
      }

      System.out.println(getName()+" finished eating");               
      
      table.releaseChopstick(ID, left);
      table.release(left);
      System.out.println(getName()+" released left chopstick");
     
      table.releaseChopstick(ID, right);
      table.release(right);
      System.out.println(getName()+" released right chopstick");
     }
  }


}
